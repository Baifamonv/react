import React from 'react';
import ReactDOM from 'react-dom';
import Provider from './Provider';
import Consumer from './Consumer';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Provider>
        <Consumer>
            <App />
        </Consumer>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
