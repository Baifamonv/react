This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Occasions to use React Context API:

When all you want is a simple state management


also you don't want to pass down the state to component where it won't be even be used 


don't want to overkill with redux

'For example, it encourages the use of immutable or persistent data structures or strict comparison of context values which might prove difficult because many common data sources rely on mutation.

Another limitation is that the new Context API only allows for a Consumer to read values from a single Provider type, one consumer to one provider. Unlike the current API, which allows a Consumer to connect to multiple Providers.'

